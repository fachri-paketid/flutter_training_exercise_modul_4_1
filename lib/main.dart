import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.pink,
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Image.asset(
                    "images/plane.png",
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  const Text(
                    "TRAVEL APP",
                    style: TextStyle(
                      color: Colors.white,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(
                    height: 35,
                  ),
                  widgetButton(
                    text: "Stays",
                    icon: Icons.apartment_sharp,
                  ),
                  widgetButton(
                    text: "Flights",
                    icon: Icons.airplanemode_active_sharp,
                  ),
                  widgetButton(
                    text: "Cars",
                    icon: Icons.car_rental_sharp,
                  ),
                  widgetButton(
                    text: "All-inclusive Vacations",
                    icon: Icons.work_sharp,
                    isFillBackgroundColor: true,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

Widget widgetButton({
  required String text,
  required IconData icon,
  bool isFillBackgroundColor = false,
}) =>
    Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: Colors.white,
        ),
        const SizedBox(
          width: 20,
        ),
        OutlinedButton(
          onPressed: () {
            print(text);
          },
          child: Text(text),
          style: OutlinedButton.styleFrom(
              primary: isFillBackgroundColor ? Colors.orange : Colors.white,
              side: const BorderSide(
                width: 2.0,
                color: Colors.white,
              ),
              minimumSize: const Size(200.0, 40.0),
              backgroundColor: isFillBackgroundColor ? Colors.white : null),
        ),
        const SizedBox(
          width: 5,
        ),
      ],
    );
